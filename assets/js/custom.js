jQuery(function ($) {

    //Preloader
    var preloader = $('.preloader');
    $(window).load(function () {
        preloader.remove();
    });

    $('.slimmenu').slimmenu({
        resizeWidth: '992',
        collapserTitle: 'Menu',
        animSpeed: 'medium',
        indentChildren: true,
        childrenIndenter: '<i class="fa fa-angle-right" aria-hidden="true"></i>',
        expandIcon: '<i class="fa fa-plus" aria-hidden="true"></i>',
        collapseIcon: '<i class="fa fa-minus" aria-hidden="true"></i>'
    });
});
